
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;  (package-initialize)

(put 'dired-find-alternate-file 'disabled nil)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; load-theme early
;;(load-theme 'material)
(load-theme 'tango-dark)

;; Custom packages to install
(defvar myPackages
  '(use-package
    magit               ;; git integration for emacs
    org                 
    omnisharp           ;; omnisharp port for emacs
    company
    elpy
    graphviz-dot-mode
    py-autopep8
    material-theme
    dockerfile-mode
    )
  )

;; Scans the list in myPackages
;; If the package listed is not already installed, install it
(mapc #'(lambda (package)
          (unless (package-installed-p package)
            (package-install package)))
      myPackages)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("ab0f81acbf7510140b0e592523eb12424b96da7c52bd3e0318c4456d114015a6" "b06b2483f9fb7bfceb371c8341c0f20e3a38fecd7af7ac1c67bfa028aed9f45c" "afd761c9b0f52ac19764b99d7a4d871fc329f7392dfc6cd29710e8209c691477" "d4f8fcc20d4b44bf5796196dbeabec42078c2ddb16dcb6ec145a1c610e0842f3" default)))
 '(package-selected-packages
   (quote
    (ox-jira htmlize markdown-preview-mode markdown-mode restclient yaml-mode graphviz-dot-mode hungry-delete use-package org-beautify-theme vs-dark-theme vs-light-theme py-autopep8 elpy company omnisharp magit))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Enable omnisharp
(add-hook 'csharp-mode-hook 'omnisharp-mode)
(add-hook 'csharp-mode-hook #'flycheck-mode)


;; Enable elpy
(elpy-enable)

;; Enable Flycheck
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Enable autopep8
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)
;; Enable linenumbers for python files
(add-hook 'elpy-mode-hook 'linum-mode)

;; Enable company autocompletion
(eval-after-load
 'company
 '(add-to-list 'company-backends 'company-omnisharp))

(add-hook 'csharp-mode-hook #'company-mode)
(put 'erase-buffer 'disabled nil)

;; Enable org-mode languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (python . t)
   ))

;; Create custom functions
(defun grandle/kill-inner-word ()
  "Kill the word you are currently in"
  (interactive)
  (backward-word)
  (kill-word 1)
  )
(global-set-key (kbd "C-c w k") 'grandle/kill-inner-word)

(global-set-key (kbd "C-c l k") 'kill-whole-line)

;; Setup remote connections
(defun grandle/shell-open-remote (host)
  "Open a shell to a remote host"
  (interactive "sHostname: ")
  (let ((default-directory (format "/ssh:administrator@%s:" host)))
    (shell host)))

(defun grandle/dired-open-remote (host path)
  "Open dired on a remote host"
  (dired (format "/ssh:administrator@%s:%s" host path)))

(defun grandle/shell-open-fm ()
  (interactive)
  (let ((fm-host "fs-grandle-05"))
  (grandle/shell-open-remote fm-host)))

(defun grandle/shell-open-robots ()
  (interactive)
  (let ((robot-host "fs-grandle-07"))
  (grandle/shell-open-remote robot-host)))

(defun grandle/shell-open-all()
  (interactive)
  (let ((hosts '("fs-grandle-05" "fs-grandle-07")))
    (while hosts
      (grandle/shell-open-remote (car hosts))
      (setq hosts (cdr hosts)))))

(use-package hungry-delete
  :ensure t
  :config
  (global-hungry-delete-mode))

(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 4))

(use-package company-graphviz-dot)

;; Create macros
(fset 'org-mode-src-sh
   "#+BEGIN_SRC\C-m#+END_SRC\C-[OA\C-[OF sh")

;; ASTHETIC
;; Disable menu bar
(menu-bar-mode 0)

;; Keeping working dirs tidy
(let ((backup-dir "~/.saves")
      (auto-saves-dir "~/tmp/emacs/auto-saves"))
  (dolist (dir (list backup-dir auto-saves-dir))
    (when (not (file-directory-p dir))
      (make-directory dir t)))
  (setq backup-directory-alist `(("." . ,backup-dir))
	auto-saves-dir auto-saves-dir))

(setq backup-by-copying t
      delete-old-version t
      version-control t
      kept-new-version 5
      kept-old-versions 2)

;; Revert buffers on changes
(setq global-auto-revert-mode t)
